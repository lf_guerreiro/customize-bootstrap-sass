/* gulpfile.js */
var 
    gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload;
    sass = require('gulp-sass');

// source and distribution folder
var
    source = 'src/',
    dest = 'dist/';

// Bootstrap scss source
var bootstrapSass = {
        in: './node_modules/bootstrap-sass/'
    };

// fonts
var fonts = {
        in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
        out: dest + 'fonts/'
    };

 // css source file: .scss files
var css = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',
    sassOpts: {
        outputStyle: 'nested',
        precision: 8,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};


gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

gulp.task('templates', function () {
    return gulp.src('demo.html')
        .pipe(reload({stream: true}));
}); 

gulp.task('serve', ['sass', 'templates'], function() {

    browserSync.init({
        server: {
            baseDir: "./",
            index: "demo.html"
        }
    });

    gulp.watch(css.watch, ['sass']);
    gulp.watch('./demo.html', ['templates']);

});

// compile scss
gulp.task('sass', ['fonts'], function () {
    return gulp.src(css.in)
        .pipe(sass(css.sassOpts))
        .pipe(gulp.dest(css.out))
        .pipe(browserSync.stream());
});


// default task
gulp.task('default', ['serve']);
